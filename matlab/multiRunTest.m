%% Test on all braking runs

folder = 'C:\SBC\SDV\Exports\20200924-133204_Activities_AuburnBrakingStudy_PROCESSED';
files = dir(folder); files(1) = []; files(1) = [];
results = table();

% setups for each run
low = [1,2];
high = [3,4];
production = [5,6];
falling = [7,8];
rising = [9,10,11];

for i=1:length(files)
    if nnz(low == i)
        configName = 'low';
    elseif nnz(high == i)
        configName = 'high';
    elseif nnz(falling == i)
        configName = 'falling';
    else
        configName = 'rising';
    end
    
    data = importDataCSV(fullfile(files(i).folder,files(i).name));
    data.FBrakeF = data.FBrakeF - data.FBrakeF(1);
    data.FBrakeR = data.FBrakeR - data.FBrakeR(1);
    thisResult = brakingAnalysis(data,2,configName);
    results(i,:) = thisResult;
end


%% Test visualization Function












