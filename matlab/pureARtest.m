%% Anti-Rise Rear Brake Until Traction Loss Test
% braking on a 15ft section seated with saddle at a climbing height,
% runs of Rising and Production curves
% speed: Imports.General.trigger2speed

%% import data
sensors = SensorDB.load;
risingDir = 'C:\repo\studies\braking\data\backyardBenchmark\RISING_E1-361';
stockDir = 'C:\repo\studies\braking\data\backyardBenchmark\stock';

risingFiles = dir(risingDir); stockFiles = dir(stockDir);
risingFiles(1) = []; risingFiles(1) = []; stockFiles(1) = []; stockFiles(1) = [];

% rising data 
risingRuns = struct();
for i=1:length(risingFiles)
    risingRuns(i).run = i;
    risingRuns(i).data = SDL.importBinFile(fullfile(risingFiles(i).folder,risingFiles(i).name));
    
    % calibrate shock pot
    shockGain = .1/(2^13);
    risingRuns(i).data.A2_1 = shockGain .* (risingRuns(i).data.A2_1 - risingRuns(i).data.A2_1(1));
    
    % calibrate rear force sensor
    strainGauge = sensors(67);
    risingRuns(i).data.A4_1 = strainGauge.CalibrationData.gain .* (risingRuns(i).data.A4_1 - ...
        risingRuns(i).data.A4_1(1));
    risingRuns(i).data.A4_1(risingRuns(i).data.A4_1 < 0) = 0;

    % wheel speed
    frontTrigger = risingRuns(i).data.A3_2;
    rearTrigger = risingRuns(i).data.A4_2;
    thresholdFront = ((max(frontTrigger) - min(frontTrigger))/2) + min(frontTrigger);
    thresholdRear = ((max(rearTrigger) - min(rearTrigger))/2) + min(rearTrigger);
    time = risingRuns(i).data.time; numMagnets = 2; slipFactor = 0.8;
    
    isRawSignal = true; 
    risingRuns(i).data.vF = Imports.General.trigger2speed(time,frontTrigger,thresholdFront,'rise',numMagnets,2.31409714863,.1);
    risingRuns(i).data.vR = Imports.General.trigger2speed(time,rearTrigger,thresholdRear,'rise',numMagnets,2.31409714863,.1);
    risingRuns(i).data.isWheelSlip = rearWheelSlip(frontTrigger,rearTrigger,time,numMagnets,slipFactor,isRawSignal);
end
% stock data 
stockRuns = struct();
for i=1:length(stockFiles)
    stockRuns(i).run = i;
    stockRuns(i).data = SDL.importBinFile(fullfile(stockFiles(i).folder,stockFiles(i).name));
    
    % calibrate shock pot
    shockGain = .1/(2^13);
    stockRuns(i).data.A2_1 = shockGain .* (stockRuns(i).data.A2_1 - stockRuns(i).data.A2_1(1));
    
    % calibrate rear force sensor
    strainGauge = sensors(67);
    stockRuns(i).data.A4_1 = strainGauge.CalibrationData.gain .* (stockRuns(i).data.A4_1 - ...
        stockRuns(i).data.A4_1(1));
    stockRuns(i).data.A4_1(stockRuns(i).data.A4_1 < 0) = 0;
    
    % wheel speed
    frontTrigger = stockRuns(i).data.A3_2;
    rearTrigger = stockRuns(i).data.A4_2;
    thresholdFront = ((max(frontTrigger) - min(frontTrigger))/2) + min(frontTrigger);
    thresholdRear = ((max(rearTrigger) - min(rearTrigger))/2) + min(rearTrigger);
    time = stockRuns(i).data.time; numMagnets = 2; slipFactor = 0.8;
    
    isRawSignal = true; 
    stockRuns(i).data.vF = Imports.General.trigger2speed(time,frontTrigger,thresholdFront,'rise',numMagnets,2.31409714863,.1);
    stockRuns(i).data.vR = Imports.General.trigger2speed(time,rearTrigger,thresholdRear,'rise',numMagnets,2.31409714863,.1);
    stockRuns(i).data.isWheelSlip = rearWheelSlip(frontTrigger,rearTrigger,time,numMagnets,slipFactor,isRawSignal);
end
    
clearvars -except stockRuns risingRuns
   
%% start analysis 
figure
plot(risingRuns(1).data.time,risingRuns(1).data.A2_1 .* 1000);
ylabel('Shock Travel [mm]');
hold on; yyaxis right; grid on; grid minor; title('Rising');
plot(risingRuns(1).data.time,risingRuns(1).data.A4_1);
ylabel('Brake Force [N]');
xlabel('Time [s]');

figure
plot(stockRuns(1).data.time,stockRuns(1).data.A2_1 .* 1000);
ylabel('Shock Travel [mm]');
hold on; yyaxis right; grid on; grid minor; title('Stock');
plot(stockRuns(1).data.time,stockRuns(1).data.A4_1);
ylabel('Brake Force [N]');
xlabel('time [s]');

%% Scatter plot
figure
scatter(risingRuns(1).data.A4_1,risingRuns(1).data.A2_1.*1000);




