%% Auburn Brake Testing 9/23/20

%% clear and close
ccc

%% Import Data
% %LOW A4-L2
% files = SDL.importData('DownloadLocation','C:\repo\studies\braking\data\92320_Auburn_Braking_Study\LOW_A4-L2');
% %test data
% data = SDL.importBinFile(files{1});

%% import CSV's
myDir = 'C:\SBC\SDV\Exports\20201106-123002_Activities_Campus_BrakingMule_Matt';
files = dir(myDir);
files(1) = []; files(1) = [];

runsheet = struct();
for i=1:length(files)
    runsheet(i).data = importDataCSV(fullfile(files(i).folder,files(i).name));
end
runsheet(1).config = 'RISING: E1-361'; runsheet(2).config = 'RISING: E1-361';
runsheet(3).config = 'Production'; runsheet(4).config = 'Production';

%% import CSV's RAW
fsRAW = 400;
myDir = 'C:\SBC\SDV\Exports\20200925-095322_Activities_AuburnBrakingStudy_RAW';
files = dir(myDir);
files(1) = []; files(1) = [];

runsheetRAW = struct();
for i=1:length(files)
    runsheetRAW(i).data = importDataCSV(fullfile(files(i).folder,files(i).name));
end
runsheetRAW(1).config = 'LOW: A4-402'; runsheetRAW(2).config = 'LOW: A4-402';
runsheetRAW(3).config = 'HIGH: CS-350'; runsheetRAW(4).config = 'HIGH: CS-350';
runsheetRAW(5).config = 'Production'; runsheetRAW(6).config = 'Production';
runsheetRAW(7).config = 'FALLING: A4-480'; runsheetRAW(8).config = 'FALLING: A4-480';
runsheetRAW(9).config = 'RISING: E1-361'; runsheetRAW(10).config = 'RISING: E1-361';
runsheetRAW(11).config = 'RISING: E1-361';

%% Apply Strain Gauge Gains and Offsets
for i=1:length(runsheet)
%     frontGain = 3.52; % part number V2:2
%     rearGain = 3.40; % part nuber V2:1
% 
%     % DO MEAN of START
%     data.A3_1 = data.A3_1 - data.A3_1(1);
%     data.A4_1 = data.A4_1 - data.A4_1(1);
% 
%     data.A3_1 = data.A3_1 * frontGain;
%     data.A4_1 = data.A4_1 * rearGain;
      runsheet(i).data.FBrakeF = runsheet(i).data.FBrakeF - runsheet(i).data.FBrakeF(1);
      runsheet(i).data.FBrakeR = runsheet(i).data.FBrakeR - runsheet(i).data.FBrakeR(1);
end

%% comment out output test
% %% TEST OUTPUTS
% %% BRAKE FORCE
% figure
% subplot(2,1,1)
% plot(data.time,data.A3_1)
% grid on; grid minor;
% title('Brake Force: Front');
% xlabel('Time [sec]');
% ylabel('Force [N]');
% legend('Front','Rear');
% 
% hold on
% subplot(2,1,2);
% plot(data.time,data.A4_1)
% grid on; grid minor;
% title('Brake Force: Rear');
% xlabel('Time [sec]');
% ylabel('Force [N]');
% legend('Front','Rear');
% 
% %% Wheel Slip
% 
% %% Suspension Travel
% frontGain = 200/(2^13);
% rearGain = 100/(2^13);
% 
% data.A1_1 = data.A1_1 * frontGain;
% data.A2_1 = data.A2_1 * frontGain;
% 
% data.A1_1 = data.A1_1 - data.A1_1(1);
% data.A2_1 = data.A2_1 - data.A2_1(1);
% 
% %% Plot susp travel
% figure
% subplot(2,1,1)
% plot(data.time,data.A1_1)
% grid on; grid minor;
% title('Fork Travel');
% xlabel('Time [sec]');
% ylabel('Displacement [mm]');
% 
% hold on
% subplot(2,1,2);
% plot(data.time,data.A2_1)
% hold on
% plot(data.time,data.A2_1)
% grid on; grid minor;
% title('Shock Travel');
% xlabel('Time [sec]');
% ylabel('Displacement [mm]');

%% Brake Force check
figure
plot(runsheet(1).data.t,runsheet(1).data.FBrakeF)
hold on
plot(runsheet(1).data.t,runsheet(1).data.FBrakeR)

%% Check for is Braking and isWheelSlip
fs = 1/(runsheet(i).data.t(2) - runsheet(i).data.t(1));

for i=1:length(runsheet)
    runsheet(i).data.isBrakingF = logical(runsheet(i).data.FBrakeF > 150);
    runsheet(i).data.isBrakingR = logical(runsheet(i).data.FBrakeR > 150);
    runsheet(i).data.isBraking = logical(runsheet(i).data.isBrakingF > 0 | runsheet(i).data.isBrakingR > 0);
    runsheet(i).data.isRearWheelSlip = rearWheelSlip(runsheet(i).data.vF,runsheet(i).data.vR,...
        runsheet(i).data.t,2,0.8,false);  
    
%     % plot braking logicals
%     figure
%     subplot(2,1,1)
%     plot(runsheet(i).data.t,runsheet(i).data.isRearWheelSlip .* runsheet(i).data.isBraking);
%     title(['Rear wheel slip while braking, run: ' num2str(i)]);
%     subplot(2,1,2)
%     plot(runsheet(i).data.t,runsheet(i).data.isRearWheelSlip);
%     title(['Rear wheel slip total, run: ' num2str(i)]);
%     figure
end

gyroTest = runsheet(i).data.wyB .* runsheet(i).data.isBrakingR;

%% numerical results
results = struct();
results(1).config = 'RISING: E1-361';
results(2).config = 'Production';

%% results statistical values
resultsStats = struct();
resultsStats(1).config = 'RISING: E1-361';
resultsStats(2).config = 'Production';

%% Numerical results computation
j = 1;
for i=1:2
    %% PITCH MOVEMENTS
    results(i).pitchRateRMS = mean([rms(runsheet(j).data.wyB .* runsheet(j).data.isBrakingR),...
        rms(runsheet(j+1).data.wyB .* runsheet(j+1).data.isBrakingR)]);
    resultsStats(i).pitchRateRMSSE = std([rms(runsheet(j).data.wyB .* runsheet(j).data.isBrakingR),...
        rms(runsheet(j+1).data.wyB .* runsheet(j+1).data.isBrakingR)])/sqrt(2);

    %% TIME WHEEL SLIP WHILE BRAKING
    brakingSlip1 = nnz(runsheet(j).data.isRearWheelSlip .* runsheet(j).data.isBrakingR) / fs;
    brakingSlip2 = nnz(runsheet(j+1).data.isRearWheelSlip .* runsheet(j+1).data.isBrakingR) / fs;
    results(i).timeSlipWhileBraking = mean([brakingSlip1 brakingSlip2]);
    resultsStats(i).timeSlipWhileBrakingSE = std([brakingSlip1 brakingSlip2])/sqrt(2);

    %% TIME DH 
%     [logEdge1, indEdge1] = findEdges(runsheetRAW(j+1).data.D2_1);
%     [logEdge2, indEdge2] = findEdges(runsheetRAW(j+1).data.D2_1);
%     timeTotal1 = (indEdge1(end) - indEdge1(1)) / fsRAW;
%     timeTotal2 = (indEdge2(end) - indEdge2(1)) / fsRAW;
%     timeDH1 = timeTotal1 - runsheet(j).climbTime;
%     timeDH2 = timeTotal2 - runsheet(j+1).climbTime;
%     results(i).timeDH = mean([timeDH1 timeDH2]);
%     resultsStats(i).timeDHSE = std([timeDH1 timeDH2])/sqrt(2);
%     
    %% Time Braking
    results(i).timeBrakingBoth = mean([nnz(runsheet(j).data.isBraking)/fs...
        nnz(runsheet(j+1).data.isBraking)/fs]);
    % standard error
    resultsStats(i).timeBrakingBothSE = std([nnz(runsheet(j).data.isBraking)/fs...
        nnz(runsheet(j+1).data.isBraking)/fs])/sqrt(2);
    % time braking with front brake
    results(i).timeBrakingFront = mean([nnz(runsheet(j).data.isBrakingF)/fs...
        nnz(runsheet(j+1).data.isBrakingF)/fs]);
    % standard error
    resultsStats(i).timeBrakingFrontSE = std([nnz(runsheet(j).data.isBrakingF)/fs...
        nnz(runsheet(j+1).data.isBrakingF)/fs])/sqrt(2);
    % time braking rear brake
    results(i).timeBrakingRear = mean([nnz(runsheet(j).data.isBrakingR)/fs...
        nnz(runsheet(j+1).data.isBrakingR)/fs]);
    %standard error
    resultsStats(i).timeBrakingRearSE = std([nnz(runsheet(j).data.isBrakingR)/fs...
        nnz(runsheet(j+1).data.isBrakingR)/fs])/sqrt(2);
    
    %% Peak Brake Force Front
    results(i).peakBrakeForceFront = mean([max(runsheet(j).data.FBrakeF) max(runsheet(j+1).data.FBrakeF)]);
    results(i).peakBrakeForceRear = mean([max(runsheet(j).data.FBrakeR) max(runsheet(j+1).data.FBrakeR)]);
    % standard errors
    resultsStats(i).peakBrakeForceFrontSE = std([max(runsheet(j).data.FBrakeF) max(runsheet(j+1).data.FBrakeF)])/sqrt(2);
    resultsStats(i).peakBrakeForceRearSE = std([max(runsheet(j).data.FBrakeR) max(runsheet(j+1).data.FBrakeR)])/sqrt(2);
    
    %% HIGH dF/dt BRAKING EVENTS
    FBrakeFSmooth1 = smoothdata(runsheet(j).data.FBrakeF,'gaussian',100);
    FBrakeRSmooth1 = smoothdata(runsheet(j).data.FBrakeR,'gaussian',100);
    dFdtBrakeF1 = diff(FBrakeFSmooth1);
    dFdtBrakeR1 = diff(FBrakeRSmooth1);
    highDFDTRlog1 = logical(dFdtBrakeR1 > 10);
    FBrakeFSmooth2 = smoothdata(runsheet(j+1).data.FBrakeF,'gaussian',100);
    FBrakeRSmooth2 = smoothdata(runsheet(j+1).data.FBrakeR,'gaussian',100);
    dFdtBrakeF2 = diff(FBrakeFSmooth2);
    dFdtBrakeR2 = diff(FBrakeRSmooth2);
    highDFDTRlog2 = logical(dFdtBrakeR2 > 10);
    results(i).highForceSlopePitchRateRMS = mean([rms(runsheet(j).data.wyB .* [highDFDTRlog1; 0]),...
        rms(runsheet(j+1).data.wyB .* [highDFDTRlog2; 0])]);
    results(i).highSlopeForceTime = mean([nnz(highDFDTRlog1)/400,...
        nnz(highDFDTRlog2)/400]);
    % standard error
    resultsStats(i).highForceSlopePitchRateRMSSE = std([rms(runsheet(j).data.wyB .* [highDFDTRlog1; 0]),...
        rms(runsheet(j+1).data.wyB .* [highDFDTRlog2; 0])])/sqrt(2);

    %% iterate counter
    j = j + 2;
end

%% zzz

FBrakeFSmooth = smoothdata(runsheet(i).data.FBrakeF,'gaussian',100);
FBrakeRSmooth = smoothdata(runsheet(i).data.FBrakeR,'gaussian',100);

% figure
% plot(runsheet(i).data.t,FBrakeFSmooth);
% figure
% plot(runsheet(i).data.t,FBrakeRSmooth);

dFdtBrakeF = diff(FBrakeFSmooth);
dFdtBrakeR = diff(FBrakeRSmooth);

figure
subplot(2,1,1)
title('Front Brake Force');
plot(runsheet(i).data.t,FBrakeFSmooth)
hold on
subplot(2,1,2)
title('Front Brake Force Derivative (momentum?)');
plot(runsheet(i).data.t,[dFdtBrakeF; 0])
hold on
y = ones(length(runsheet(i).data.t),1) * 30;
plot(runsheet(i).data.t,y)

figure
subplot(2,1,1)
grid on; grid minor
title('Rear Brake Force');
xlabel('time [sec]');
ylabel('Force [N]');
hold on
plot(runsheet(i).data.t,FBrakeRSmooth)
subplot(2,1,2)
plot(runsheet(i).data.t,[dFdtBrakeR; 0])
hold on
y = ones(length(runsheet(i).data.t),1) * 10;
plot(runsheet(i).data.t,y)
title('Rear Brake Force Time Derivative');
xlabel('time [sec]');
ylabel('dF/dt [N/s]');
grid on; grid minor

% highDFDTRlog = logical(dFdtBrakeR > 10);
% plot(highDFDTRlog);
% rms(runsheet(i).data.wyB .* [highDFDTRlog; 0]);

%% do high force slope pitch analysis for all runs
% for i=1:length(runsheet)
%     FBrakeFSmooth = smoothdata(runsheet(i).data.FBrakeF,'gaussian',100);
%     FBrakeRSmooth = smoothdata(runsheet(i).data.FBrakeR,'gaussian',100);
%     dFdtBrakeF = diff(FBrakeFSmooth);
%     dFdtBrakeR = diff(FBrakeRSmooth);
%     highDFDTRlog = logical(dFdtBrakeR > 10);
%     rms(runsheet(i).data.wyB .* [highDFDTRlog; 0]);
% end

%% Create BAR GRAPH Visuals for numerical results    
for i=2:numel(fieldnames(results))
    myFig = figure
    fields = categorical({results(1).config,results(2).config});
    fields = reordercats(fields,{results(1).config,results(2).config});
    X = 1:2;
    r = struct2cell(results);
    standardError = struct2cell(resultsStats);
    errY = [standardError{i-1,1,1} standardError{i-1,1,2} ];
    errY = [0 0];
    Y = [r{i,1,1} r{i,1,2}];
    barwitherr(errY,X,Y)
    set(gca,'xticklabel',fields)
    xtickangle(25)
    fieldNames = fieldnames(results);
    title(fieldNames{i})
    grid on; grid minor;
    
    if contains(lower(fieldNames{i}),'time')
        ylabel('Time [sec]');
    elseif contains(lower(fieldNames{i}),'force')
        ylabel('Force [N]');
    elseif contains(lower(fieldNames{i}),'pitch')
        ylabel('Pitch Rate [rad/sec]');
    end   
    
    saveas(myFig,['C:\repo\studies\braking\matlab\figures\mattsCampusRideTribute\' fieldNames{i} '.jpg']);
end