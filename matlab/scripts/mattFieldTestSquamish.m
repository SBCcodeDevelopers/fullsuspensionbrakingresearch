%% enduro Braking analysis 
% S4 Enduro
ccc

%% load bikes
bikes = ComponentDB.load('ComponentLibraryPath','C:\SBC\ComponentDB','PullDataFromRemote',false);

%% get enduro
enduro = bikes.bikeConfigs(98);
enduro.Frame.CurrentGeo = enduro.Frame.Geometry(3);

%% estimate Matt's CoM 
riderMass = 83.91;
riderHeight = 1.95;
rider = bodyLengthAndMass(riderHeight, riderMass);
[com, riderJointLocs] = estimateCoM(enduro,rider);

%% plot Matt and bike
plotBikeSkeleton(enduro.Frame.CurrentGeo);
plotBikeSkeleton(enduro.Frame.CurrentGeo,'IncludeRider',riderJointLocs);

%% get braking mule info
load('brakeConfig.mat');
myBrakeConfig = brakeConfigs;
myBrakeConfig.frontSetting = brakeConfigs.frontSetting.E1;
myBrakeConfig.rodLength = myBrakeConfig.rodLength.L2; % 361mm

%% compute anti-rise curve
antiriseStock = computeAntirise(enduro,com.total.x)
antiriseRising = computeAntirise(enduro,com.total.x,myBrakeConfig)


