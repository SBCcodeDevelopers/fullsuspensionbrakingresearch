
%% wheel slip with feedback button
data = SDL.importBinFile('20200922-122315_00_sbc9746-16_V1p004p001.bin');
%% test wheel slip
data = SDL.importBinFile('20200922-120326_00_sbc9746-16_V1p004p001.bin');

%% import data
data = SDL.importBinFile('20200922-114847_03_sbc9746-16_V1p004p001.bin');

%% calc wheel speeds
% fix wheel circumference (NOT hardcoded)
vF = Imports.General.trigger2speed(data.time,data.A3_2,7000,'rise',2,2.31409714863,.1);
vR = Imports.General.trigger2speed(data.time,data.A4_2,7000,'rise',2,2.31409714863,.1);

vFsmooth = smoothdata(vF,'movmean',500);
vRsmooth = smoothdata(vR,'movmean',500);

%% plot
figure
% plot(vF);
hold on;
plot(data.time,vFsmooth);
hold on;
% plot(vR);
plot(data.time,vRsmooth);
legend('front','rear');
grid on
grid minor
%% plot skid button
plot(data.D2_1);

%% test whee slip
slipFactor = 0.8;
isWheelSlip = logical(slipFactor * vFsmooth > vRsmooth);

%% plot slip
yyaxis right
plot(data.time,isWheelSlip);

