function results = brakingAnalysis(data,numMagnets,configName,varargin)
%brakingAnalysis runs chassis stability and rear wheel slip analysis on field test data
% This function takes a struct following the format of exported data from
% the SDV (processed and calibrated to CSV) and runs a wheel slip and
% ptich rate rms under "highly dynamic" braking events
%
%
% SYNTAX:
%   argOut1 = brakingAnalysis(argIn1,argIn2)
%   [argOut1,argOut2] = brakingAnalysis(argIn1,argIn2)
%   [argOut1,argOut2] = brakingAnalysis(argIn1,argIn2,varargin1,...)
%   [argOut1,argOut2] = brakingAnalysis(argin1,argIn2,Name,Value)
%
% REQUIRED INPUTS:
%   - argIn1 - Describe the first input argument including size and class
%   - exampleArgIn - A double or an (m x n) array of doubles, that
%   represents something relevant to the problem this function solves.
%
% OPTIONAL INPUTS:
%   - argOption1 - An optional input described in the same manner as the
%   required inputs. Be sure to describe the default behavior.
%
%   Advanced users may want to implement optional inputs as "Name,Value"
%   pairs. This can be done using the "inputParser" class. 
%
% OUTPUTS:
%   - argOut1 - Describe the first output argument including size and class
%   - exampleArgOut - A (m x 1) cell array of strings containing some
%   information relevant to the problem this function solves.
%
% EXAMPLES:
%   If required, provide examples of how to use the function here.
%
% See also newFunction, inputParser, function.

% Copyright 2021 Specialized Bicycle Components, Inc.
% Author: mmorrison, mmorrison@specialized.com 

%% Required Input Validation
if nargin < 2
    error('This function requires at least 2 input arguments!')
end

%% Optional Input Validation
if nargin > 2
    % Check the validity of the user inputs here.
    % "Name,Value" pairs can be implemented with the "inputParser" class
end

%% Config Name
if ~exist('configName')
    configName = 'default';
end

%% Main Function Logic
varNames = {'configName','tBrakingBoth','tBrakingFront','tBrakingRear','tSlip'...
    'wyRMS_braking','wyRMS_highBraking','tHighBraking','sagBraking','sagHighBraking'};
results = table({configName},nan,nan,nan,nan,nan,nan,nan,nan,nan,'VariableNames',varNames);

fs = round(1/(data.t(2) - data.t(1))); % sampling frequency

%% Max brake force
results.FBrakeFMax = max(data.FBrakeF);
results.FBrakeRMax = max(data.FBrakeR);

%% Check for is braking
brakingThreshold = 150; % 150N of brake force, ensures omission of sensor drift/noise
data.isBrakingF = logical(data.FBrakeF > brakingThreshold);
data.isBrakingR = logical(data.FBrakeR > brakingThreshold);
data.isBraking = logical(data.isBrakingF > 0 | data.isBrakingR > 0);

%% Time Braking
results.tBrakingBoth = nnz(data.isBraking) / fs; % both brakes
results.tBrakingFront = nnz(data.isBrakingF) / fs; % front brake
results.tBrakingRear = nnz(data.isBrakingR) / fs; % rear brake

%% Wheel slip
slipFactor = 0.8; % determined experimentally by Matt Morrison
data.isRearWheelSlip = rearWheelSlip(data.vF,data.vR,data.t,numMagnets,slipFactor,false);
results.tSlip = nnz(data.isRearWheelSlip .* data.isBrakingR) / fs;

%% Chassis Stability under braking
results.wyRMS_braking = rms(data.wyB .* data.isBrakingR);

%% Chassis Stability under "highly dynamic braking"

FBrakeFSmooth = smoothdata(data.FBrakeF,'gaussian',100);
FBrakeRSmooth = smoothdata(data.FBrakeR,'gaussian',100);

dFdtBrakeF = gradient(FBrakeFSmooth,data.t); % TODO: should we look at high front braking?
dFdtBrakeR = gradient(FBrakeRSmooth,data.t);

dFdtThreshold = 2000; %N/s

highDFDTRlog = logical(dFdtBrakeR > dFdtThreshold);

results.wyRMS_highBraking = rms(data.wyB .* highDFDTRlog);
results.tHighBraking = nnz(highDFDTRlog) / fs;

%% Sag (if available)
if nnz(strcmp('sSuspRWheelNorm',data.Properties.VariableNames))
    % Sag during braking (if travel data available)
    results.sagBraking = mean(nonzeros(data.sSuspRWheelNorm .* data.isBrakingR));
    
    % Sag during "highly dynamic braking"
    results.sagHighBraking = mean(nonzeros(data.sSuspRWheelNorm .* highDFDTRlog));
end


end % EOF [brakingAnalysis.m]
