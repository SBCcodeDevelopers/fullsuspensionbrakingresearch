function isWheelSlip = rearWheelSlip(frontTrigger,rearTrigger,time,numMagnets,slipFactor,isRawSignal)
%rearWheelSlip A one line description of what this code does
% A more detailed explanaion of this code goes here.
%
%
% SYNTAX:
%   argOut1 = rearWheelSlip(argIn1,argIn2)
%   [argOut1,argOut2] = rearWheelSlip(argIn1,argIn2)
%   [argOut1,argOut2] = rearWheelSlip(argIn1,argIn2,varargin1,...)
%   [argOut1,argOut2] = rearWheelSlip(argin1,argIn2,Name,Value)
%
% REQUIRED INPUTS:
%   - argIn1 - Describe the first input argument including size and class
%   - exampleArgIn - A double or an (m x n) array of doubles, that
%   represents something relevant to the problem this function solves.
%
% OPTIONAL INPUTS:
%   - argOption1 - An optional input described in the same manner as the
%   required inputs. Be sure to describe the default behavior.
%
%   Advanced users may want to implement optional inputs as "Name,Value"
%   pairs. This can be done using the "inputParser" class. 
%
% OUTPUTS:
%   - argOut1 - Describe the first output argument including size and class
%   - exampleArgOut - A (m x 1) cell array of strings containing some
%   information relevant to the problem this function solves.
%
% EXAMPLES:
%   If required, provide examples of how to use the function here.
%
% See also newFunction, inputParser, function.

% Copyright 2020 Specialized Bicycle Components, Inc.
% Author: mmorrison, mmorrison@specialized.com 

%% Required Input Validation
% if nargin < 2
%     error('This function requires at least 2 input arguments!')
% end

%% Optional Input Validation
% if nargin > 2
%     % Check the validity of the user inputs here.
%     % "Name,Value" pairs can be implemented with the "inputParser" class
% end


%% Main Function Logic
thresholdFront = ((max(frontTrigger) - min(frontTrigger))/2) + min(frontTrigger);
thresholdRear = ((max(rearTrigger) - min(rearTrigger))/2) + min(rearTrigger);

% if raw signal, then calc wheel speed from it, otherwise skip this step
if isRawSignal
    % trigger2speed coppied from Marcel's SDV, imports.general.trigger2speed
    vF = trigger2speed(time,frontTrigger,thresholdFront,'rise',numMagnets,2.31409714863,.1);
    vR = trigger2speed(time,rearTrigger,thresholdRear,'rise',numMagnets,2.31409714863,.1);
else
    vF = frontTrigger; vR = rearTrigger;
end

vFsmooth = smoothdata(vF,'movmean',500);
vRsmooth = smoothdata(vR,'movmean',500);

%% calc wheel slip
isWheelSlip = logical(slipFactor * vFsmooth > vRsmooth);

end % EOF [rearWheelSlip.m]
