%% brake struct creation
ccc;
brakeTable  = readtable('brakeConfigsCSV.csv');
brakeConfigs = struct();
brakeConfigs.frontSetting = struct();
brakeConfigs.rodLength = struct();
brakeConfigs.diameter = 0.20616;
d = 1;
for i=1:5
    if i==1
        config = 'A';
    elseif i==2
        config = 'B';
    elseif i==3
        config = 'C';
    elseif i==4
        config = 'D';
    elseif i==5
        config = 'E';
    end

    for j=1:8
        curConfig = [config num2str(j)];
        brakeConfigs.frontSetting.(curConfig) = [brakeTable{j,d}*.001,0,brakeTable{j,d+1}*.001];
    end
    d = d + 2;
end

brakeConfigs.rodLength.L1 = .350;
brakeConfigs.rodLength.L2 = 0.361;
brakeConfigs.rodLength.L3 = 0.402;
brakeConfigs.rodLength.L4 = 0.480;

save('C:\repo\studies\braking\matlab\brakeConfig.mat','brakeConfigs')

%% test load
ccc
load('C:\repo\studies\braking\matlab\brakeConfig.mat')
        
        
        
        
        