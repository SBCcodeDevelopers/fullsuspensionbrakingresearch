function v = trigger2speed(t, rawSignal, threshold, riseOrFall, ppr, sWheel, vMin) %#ok<INUSD>
% v = trigger2speed(t, rawSignal, threshold, riseOrFall, ppr, sWheel)
%
% %ToDo - change code to use input parse
%
% Inputs
%  t         - time signal
%  rawSignal - signal that includes the pulses (logical or numerical)
%  threshold - numerical threshold value when a pulse happense, 
%              or use 'logical' if rawSignal is already boolean
%             ToDo - do I need two options like boolean or already the trigger?
%  riseOrFall - direction how signal has to pass threshold to trigger a pulse
%  ppr        - [1] pulses per wheel revolution
%  sWheel     - [m] wheel circumference
%  vMin       - minimum speed (speed below will be set to zero by taking care about total distance)
%
% Output
%  v          - speed (same dimension than "t" and "rawSignal")


%Create logical vector when a pulse happens
if isnumeric(threshold)
  [~, isPulse] = findCross(rawSignal, 'riseOrFall', riseOrFall, 'threshold', threshold);
else
  error('logical inputs for speed sensor not developed yet');
  %ToDo - take care about case that logical
end

%convert from logical vector to index, easier for some lines
idPulse = find(isPulse);
if isempty(idPulse) %there was no trigger in the signal, speed is zero for entire record
  v = zeros(size(t));
  return
end

%calc speed average between 2 trigger, first add inf as first element for tPassed to get vAvg(1)=0 and same side as idPulse
tPassed = [inf; diff(t(idPulse))];
vAvg    = (sWheel/ppr)./tPassed; %average speed of last increment
%NOTE: in theory the speed vAvg does not happen at the second trigger, but at the mean time between 1st and second
%      so we could sample back for half the meantime to be more accurate. But this could cause half indexes
%      the decision to ceil or floor the id is only a guess and for sure sometimes wrong. 
%      Decide to simplify and life with the inaccuracy
%   Example idea: idSpeed = idReed(1:end-1) + (diff(idReed)./2)

%Check for very low speed (below vMin) and set to vMin, take care that no distance is missing.
%ToDo

% CALCULATE SPEED
%build temporary time and speed vector for interpolation, add zero values at begin and end
%extend idReed by one to come back to zero speed at the end
%ToDo - not sure if this is the perfect way
idPulseExtension = max(length(t), idPulse(end) + (idPulse(end)-idPulse(end-1))); %cannot be higher than signal length
if idPulseExtension<length(t)
  tTMP = [0; t(idPulse); t(idPulseExtension); t(end)];
  vTMP = [0; vAvg;            0;                        0 ];
else
  tTMP = [0; t(idPulse); t(end)];
  vTMP = [0; vAvg;            0 ];
end

%interpolate speed as output channel with same length as input signal
v = interp1(tTMP, vTMP, t);

end %EOF [trigger2speed]

