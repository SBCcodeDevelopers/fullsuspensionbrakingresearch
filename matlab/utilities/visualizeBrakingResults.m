function [argOut1,argOut2] = visualizeBrakingResults(data)
%visualizeBrakingResults A one line description of what this code does
% A more detailed explanaion of this code goes here.
%
%
% SYNTAX:
%   argOut1 = visualizeBrakingResults(argIn1,argIn2)
%   [argOut1,argOut2] = visualizeBrakingResults(argIn1,argIn2)
%   [argOut1,argOut2] = visualizeBrakingResults(argIn1,argIn2,varargin1,...)
%   [argOut1,argOut2] = visualizeBrakingResults(argin1,argIn2,Name,Value)
%
% REQUIRED INPUTS:
%   - argIn1 - Describe the first input argument including size and class
%   - exampleArgIn - A double or an (m x n) array of doubles, that
%   represents something relevant to the problem this function solves.
%
% OPTIONAL INPUTS:
%   - argOption1 - An optional input described in the same manner as the
%   required inputs. Be sure to describe the default behavior.
%
%   Advanced users may want to implement optional inputs as "Name,Value"
%   pairs. This can be done using the "inputParser" class. 
%
% OUTPUTS:
%   - argOut1 - Describe the first output argument including size and class
%   - exampleArgOut - A (m x 1) cell array of strings containing some
%   information relevant to the problem this function solves.
%
% EXAMPLES:
%   If required, provide examples of how to use the function here.
%
% See also newFunction, inputParser, function.

% Copyright 2021 Specialized Bicycle Components, Inc.
% Author: mmorrison, mmorrison@specialized.com 

%% Required Input Validation
if nargin < 2
    error('This function requires at least 2 input arguments!')
end

%% Optional Input Validation
if nargin > 2
    % Check the validity of the user inputs here.
    % "Name,Value" pairs can be implemented with the "inputParser" class
end


%% Main Function Logic
configs = unique(data.configName);

configName = data.Properties.VariableNames(1);
configTable = table(repmat({''},length(configs),1),'VariableNames',configName);

meanNames = cellfun(@(x) [x,'_mean'],data.Properties.VariableNames(2:end),'UniformOutput',false);
stdENames = cellfun(@(x) [x,'_stdE'],data.Properties.VariableNames(2:end),'UniformOutput',false);

meanResults = array2table(nan(length(configs),length(meanNames)),'VariableNames',meanNames);
stdEResults = array2table(nan(length(configs),length(stdENames)),'VariableNames',stdENames);

meanResults = [configTable,meanResults];
stdEResults = [configTable,stdEResults];

for i=1:height(meanResults)
    thisData = data(strcmp(data.configName,configs{i}),:);  
    meanResults{i,1} = thisData.configName(1);
    for j=2:width(thisData)
        meanResults{i,j}= mean(thisData{:,j});
        stdEResults{i,j} = std(thisData{:,j})/sqrt(height(thisData));
    end
    
end

%% Plot results

for i=2:width(meanResults)
    figure;
    barwitherr(stdEResults{:,i},meanResults{:,i})
    set(gca,'xticklabel',meanResults{:,1})
    xtickangle(25) 
    thisTitle = strrep(meanResults.Properties.VariableNames{i},'_',' ');
    title(thisTitle)
    xlabel('Bike Configuration');
    if strcmpi(thisTitle(1),'t')
        ylabel('Time [s]');
    elseif strcmpi(thisTitle(1),'s')
        ylabel('Displacement [m]');
    elseif strcmpi(thisTitle(1),'w')
        ylabel('Angular Velocity [rad/sec]');
    elseif strcmpi(thisTitle(1),'f')
        ylabel('Force [N]');
    end
    grid on; grid minor;
end














end % EOF [visualizeBrakingResults.m]
